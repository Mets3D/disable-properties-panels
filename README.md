This addon adds a list of checkboxes to the drop-down menu in the top-right corner of the Properties Editor, which lets you disable each Panel:  
<img src="docs/checkboxes.png" width=500>  

### Known Issues
- When running the Reload Scripts operator, some checkboxes may stop working until Blender restart.
- You get some console spam before quitting Blender. Not sure why this happens, only makes sense if addon unregistration is multi-threaded, which wouldn't make much sense to me.

If you find any other issues, please [report them](https://gitlab.com/Mets3D/disable-properties-panels/-/issues/new)!