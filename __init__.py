# Disable Properties Panels; Blender Addon that is perfectly described by its title
# Copyright (C) 2021 Demeter Dzadik

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details:
# https://www.gnu.org/licenses/.

bl_info = {
	"name": "Disable Properties Panels",
	"author": "Demeter Dzadik",
	"version": (1,0),
	"blender": (2, 80, 0),
	"location": "Properties Editor > Arrow in the top right",
	"description": "Adds toggles to disable panels in the Properties Editor",
	"category": "Interface",
	"doc_url": "https://gitlab.com/Mets3D/disable-properties-panels",
	"tracker_url": "https://gitlab.com/Mets3D/disable-properties-panels/-/issues/new",
}

import bpy
from bpy.props import BoolProperty, StringProperty, CollectionProperty, IntProperty
from bpy.app.handlers import persistent

dpp_verbose = False

def dpp_print(s):
	"""I don't want to delete my print()s, they might be useful if this ever explodes."""
	if dpp_verbose:
		print(s)


def get_addon_panel_datas(context=None):
	"""Return a reference to the PanelData CollectionProperty stored in the addon's preferences."""
	if not context:
		context = bpy.context
	prefs = context.preferences.addons[__package__].preferences
	panel_datas = prefs.panel_datas
	return panel_datas


class PanelData(bpy.types.PropertyGroup):
	name: StringProperty(name="Blender Python class name")
	enabled: BoolProperty(name="Enabled", default=True)
	category: StringProperty(name="Corresponding Panel's bl_context")
	label: StringProperty(name="Corresponding Panel's bl_label")


class DPP_AddonPreferences(bpy.types.AddonPreferences):
	bl_idname = __package__

	panel_datas: CollectionProperty(type=PanelData)


def list_top_level_panel_classes() -> list:
	"""Return a list of top level classes currently registered in Blender.
	These are the panels which we want to be able to disable with this addon."""

	panel_classes = []
	for type_name in dir(bpy.types):
		bpy_type = getattr(bpy.types, type_name)

		required_attributes = ['bl_context', 'bl_space_type', 'bl_label']
		has_required_attributes = [hasattr(bpy_type, prop_name) for prop_name in required_attributes]
		if not all(has_required_attributes):
			# All the classes we are interested in would have these properties.
			# Could've used isinstance(x, bpy.types.Panel), but this feels safer.
			continue
		if hasattr(bpy_type, 'bl_parent_id'):
			# This filters sub-panels.
			continue
		if bpy_type.bl_label=="":
			# This filters Constraint panels for example.
			continue
		if hasattr(bpy_type, 'bl_options') and 'HIDE_HEADER' in bpy_type.bl_options:
			# This filters the top of some panels which have hidden headers, so they don't need to be hidden again.
			continue

		assert bpy_type.bl_rna.name != "", f"We failed to filter out a class which apparently has no class name? This should never happen! {bpy_type}"

		if bpy_type.bl_space_type == 'PROPERTIES':
			dpp_print(f"Found top-level Panel class: {bpy_type.bl_rna.name}")
			panel_classes.append(bpy_type)

	return panel_classes


def inject_panel_class_poll_functions(panel_classes):
	"""We inject a poll function for each class so that it checks for the corresponding
	toggle in the addon's preferences.
	This seems to require un-registering and re-registering it.
	"""

	panel_datas = get_addon_panel_datas()

	for panel_class in panel_classes:
		dpp_print(f"Unregistering top-level panel: '{panel_class}'")
		panel_data = panel_datas[panel_class.bl_rna.name]

		# Injecting poll function
		dpp_print(f"Injecting poll function into {panel_data.name}")
		if hasattr(panel_class, 'old_poll'):
			# This only happens if we are trying to inject a new poll function into a class for the second time.
			# This should ideally be avoided, and never happen.
			# For now, let's just assume that everything is fine and don't do anything.
			continue
		if hasattr(panel_class, 'poll'):
			panel_class.old_poll = panel_class.poll
		panel_class.poll = create_poll_function(panel_data, panel_class)

def create_poll_function(toggle_entry, panel_class) -> callable:
	"""Creates a function definition for a poll function that is hooked up to one 
	of the toggles stored in the addon preferences. Pretty wild.
	"""
	@classmethod
	def poll(cls, context):
		if not toggle_entry.enabled:
			return False
		elif hasattr(panel_class, 'old_poll'):
			return panel_class.old_poll(context)
		else:
			return True

	return poll

@persistent
def refresh_addon_preferences_data(dummy=None):
	"""Make sure every currently registered Properties panel has a corresponding boolean toggle
	in this addon's preferences.

	Importantly, this function must run after all Blender UI and addons have finished registering,
	so this must only be called from an app handler, eg. load_post.
	This does mean that there is no nice way to allow the addon to continue functioning after
	the Reload Scripts operator was run, which is fine. Users won't be pressing that button all the time.
	"""
	prefs = bpy.context.preferences.addons[__package__].preferences
	prefs.active_panel_data_index = 0
	panel_datas = prefs.panel_datas

	panel_classes = list_top_level_panel_classes()
	for panel_class in panel_classes:
		panel_entry = panel_datas.get(panel_class.bl_rna.name)
		if panel_entry:
			# Since lists in addon preferences are successfully saved, 
			# existing entries should not be re-created.
			continue
		new_entry = panel_datas.add()
		new_entry.name = panel_class.bl_rna.name
		new_entry.category = panel_class.bl_context
		new_entry.label = panel_class.bl_label
	
	for i, panel_data in reversed(list(enumerate(panel_datas))):
		# Let's also remove any panel data that corresponds to panels that no longer exist.
		# Just in case someone disabled the panel of an addon and then disabled that addon,
		# we don't want to hold onto that data, better to reset it.
		if panel_data.name not in dir(bpy.types):
			dpp_print(f"Removing orphaned panel data: {panel_data.name}")
			panel_datas.remove(i)


	inject_panel_class_poll_functions(panel_classes)


def draw_local_properties_panel_toggles(self, context):
	layout = self.layout
	col = layout.column()
	col.use_property_split = True
	col.use_property_decorate = False

	col.separator()
	col.label(text="Toggle Panels")
	prefs = context.preferences.addons[__package__].preferences
	panel_datas = prefs.panel_datas
	for panel_data in panel_datas:
		assert panel_data, "A Panel Data is None. This should never happen."
		if panel_data.name not in dir(bpy.types):
			dpp_print(f"This panel must've been unregistered, skipping its checkbox drawing: {panel_data.name}")
			continue
		panel_class = getattr(bpy.types, panel_data.name)
		# At this point, the panel_class we get from this getattr() is NOT the original class,
		# but out wrapped version we made in inject_panel_class_poll_functions(), which has already run
		# by the time this drawing code is executed.

		if panel_class.bl_context != context.space_data.context.lower():
			continue
		if hasattr(panel_class, 'old_poll'):
			# We saved the old poll function so we know when not to draw the checkbox for the panel.
			if not panel_class.old_poll(context):
				continue

		row = col.row()
		row.prop(panel_data, 'enabled', text="")
		row.label(text=panel_data.label)

def restore_panel_class_poll_functions():
	"""We un-do what we did in inject_panel_class_poll_functions():
	We un-register our wrapping classes, then use the reference
	that we created to the old classes to re-register them.
	"""
	panel_datas = get_addon_panel_datas()
	for panel_data in panel_datas:
		if panel_data.name not in dir(bpy.types):
			# This is probably fine - the addon just un-registered the panel before we could.
			dpp_print(f"Can't un-register class because it's not registered to begin with: {panel_data.name}")
			continue
		panel_class = getattr(bpy.types, panel_data.name)
		if hasattr(panel_class, 'old_poll'):
			# Restore the class's original poll function.
			panel_class.poll = panel_class.old_poll
			del panel_class.old_poll
		else:
			# This class didn't have a poll before, so remove it.
			del panel_class.poll

classes = [
	PanelData,
	DPP_AddonPreferences,
]

def register():
	from bpy.utils import register_class
	for c in classes:
		register_class(c)

	bpy.types.PROPERTIES_PT_options.append(draw_local_properties_panel_toggles)
	bpy.app.handlers.load_post.append(refresh_addon_preferences_data)
	
	# We do something satanic to determine where this register() function was called from.
	import inspect
	caller_name = inspect.stack()[2].function
	if caller_name == 'reset_all':
		dpp_print("Reload Scripts! I'll try re-initializing but it might fail?")	# Seems fine. Feels like it should fail tho.
		refresh_addon_preferences_data()
	elif caller_name == 'execute':
		dpp_print("Addon enabled with the checkbox! Initializing wrapper classes!")
		refresh_addon_preferences_data()
	elif caller_name == '_initialize':
		dpp_print("Addon enabled when opening Blender. Initializing should happen after file load.")

def unregister():
	from bpy.utils import unregister_class
	restore_panel_class_poll_functions()

	for c in reversed(classes):
		unregister_class(c)

	bpy.types.PROPERTIES_PT_options.remove(draw_local_properties_panel_toggles)
	bpy.app.handlers.load_post.remove(refresh_addon_preferences_data)
